#include "chandler.h"

CHandler::CHandler(QObject *parent) : QObject(parent)
{
    this->posAnt.first = 0;
    this->posAnt.second = 0;

    this->contador  = 0;
    qDebug() << "Loading GUI...";

    this->srvMousers = new QTcpServer( this );
    connect( this->srvMousers, SIGNAL( newConnection( ) ), this, SLOT( makeConnection( ) ) );

    if ( !this->srvMousers->listen( QHostAddress::Any, 7777 ) ) {
        qDebug() << "Error";

    } else {
        qDebug() << "Waiting for a conection...";

    }


}

CHandler::~CHandler()
{

}

QPair< int, int > CHandler::displacementVector( int x, int y ) {
    return QPair< int, int > ( this->posAnt.first - x, this->posAnt.second - y ) ;
}


void CHandler::sendDisplacement( int x, int y ) {
    this->contador++;
    if ( contador >= LIM_POS ) {
        if ( ( this->posAnt.first != 0 ) && ( this->posAnt.second != 0 ) ) {
            qDebug( ) << "SENDING THIS: " << x << ", " << y ;

            this->socketMousers->flush();

            QByteArray block;
            QDataStream out( &block, QIODevice::WriteOnly );
            out.setVersion( QDataStream::Qt_DefaultCompiledVersion );
            CMove m( CMove::COORD, this->displacementVector( x, y ) );
            out << m.getMovType() << m.getCoord() ;
            this->socketMousers->write( block );
            this->socketMousers->flush();
        }

        this->posAnt.first = x;
        this->posAnt.second = y;

        this->contador = 0;
    }

}


void CHandler::takeFirstTouch( int x, int y ) {
    qDebug( ) << " First Touched Surface ";
    QPair< int, int > p( x, y );
    this->posAnt = p;

}

void CHandler::sendLastTouch( int x, int y ) {
    qDebug( ) << " Out of Touched Surface ";
    CMove m( CMove::MOUSE_OUT, QPair< int, int > ( x, y )) ;

    this->socketMousers->flush();

    QByteArray block;
    QDataStream out( &block, QIODevice::WriteOnly );
    out.setVersion( QDataStream::Qt_DefaultCompiledVersion );

    out << m.getMovType() << m.getCoord() ;
    this->socketMousers->write( block );

    this->socketMousers->flush();
}

void CHandler::makeConnection( ) {
    this->socketMousers = this->srvMousers->nextPendingConnection();
    this->socketMousers->write( "Connected with client." );
    this->socketMousers->flush();
}

void CHandler::sendRightClick( ) {
    qDebug( ) << " RC ";

    this->socketMousers->flush();

    QByteArray block;
    QDataStream out( &block, QIODevice::WriteOnly );
    out.setVersion( QDataStream::Qt_DefaultCompiledVersion );
    CMove m( CMove::RIGHT_CLICK, QPair< int, int > ( 0 , 0 ) );
    out << m.getMovType() << m.getCoord() ;
    this->socketMousers->write( block );
    this->socketMousers->flush();

}

void CHandler::sendLeftClick( ) {
    qDebug( ) << " LC ";
    this->socketMousers->flush();

    QByteArray block;
    QDataStream out( &block, QIODevice::WriteOnly );
    out.setVersion( QDataStream::Qt_DefaultCompiledVersion );
    CMove m( CMove::LEFT_CLICK, QPair< int, int > ( 0 , 0 ) );
    out << m.getMovType() << m.getCoord() ;
    this->socketMousers->write( block );
    this->socketMousers->flush();
}
