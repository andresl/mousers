#ifndef CHANDLER_H
#define CHANDRER_H

#include <QObject>
#include <QPair>
#include <QTcpSocket>
#include <QTcpServer>
#include <QBuffer>
#include <QtMath>
#include <QDebug>

#include "../bean/cmove.h"

class CHandler : public QObject
{
    Q_OBJECT
public:
    explicit CHandler(QObject *parent = 0);
    ~CHandler();
    static const int LIM_POS = 10;
    QPair< int, int > displacementVector( int, int );

private:
    QPair< int, int > posAnt;
    int contador;
    QTcpServer *srvMousers;
    QTcpSocket *socketMousers;

signals:

public slots:
    void sendDisplacement( int, int );
    void takeFirstTouch( int, int );
    void sendLastTouch( int, int );
    void sendRightClick( );
    void sendLeftClick( );
    void makeConnection( );
};

#endif // CHELPER_H
