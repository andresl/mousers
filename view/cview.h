#ifndef CVIEW_H
#define CVIEW_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QtQml/QQmlComponent>
#include <QtQml/QQmlEngine>
#include <QtQml/QQmlProperty>
#include <QUrl>


class CView : public QObject
{
    Q_OBJECT
public:
    explicit CView(QObject *parent = 0);
    ~CView();

signals:

public slots:
};

#endif // CVIEW_H
