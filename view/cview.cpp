#include "cview.h"
#include <QQuickView>
#include <QDebug>
#include "chandler.h"

CView::CView(QObject *parent) : QObject(parent)
{
    QQmlEngine *e = new QQmlEngine( this );
    QQmlComponent c( e, QUrl( QStringLiteral( "qrc:/view/qml/main.qml" ) ) );
    CHandler *h = new CHandler( this );

//http://doc.qt.io/qt-5/qtqml-cppintegration-exposecppattributes.html

    if ( c.isReady() ) {
        QObject *object = c.create( );

        QObject::connect( object, SIGNAL( touchMoves( int, int ) ), h, SLOT( sendDisplacement( int, int ) ) );
        QObject::connect( object, SIGNAL( surfaceIn( int, int ) ), h, SLOT( takeFirstTouch( int, int ) ) );
        QObject::connect( object, SIGNAL( surfaceOut( int, int ) ), h, SLOT( sendLastTouch( int, int ) ) );
        QObject::connect( object, SIGNAL( cmdLeftButton_clicked( ) ), h, SLOT( sendLeftClick( ) ) );
        QObject::connect( object, SIGNAL( cmdRightButton_clicked( ) ), h, SLOT( sendRightClick( ) ) );



    } else {
        qDebug( ) << c.errorString( );
    }

}

CView::~CView() {

}

