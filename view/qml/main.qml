import QtQuick 2.0
import QtQuick.Controls 1.2
//import QtCharts 2.0
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    id: main
    width: 640
    height: 940
    property int xpos
    property int ypos
    property int counting

    property int hh
    property int mm
    property int ss


    signal touchMoves( int x, int y )
    signal surfaceOut( int x, int y )
    signal surfaceIn( int x, int y )
    signal cmdRightButton_clicked( )
    signal cmdLeftButton_clicked( )



    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        spacing: 5

        Rectangle {
            height: main.height / 2
            width: main.width
            color: "lightblue"

            Canvas {
                id: myCanvas
                height: parent.height
                width: parent.width
                anchors.fill: parent

                MouseArea{
                    anchors.fill: parent
                    id: canvasSurface
                    onPositionChanged: main.touchMoves( canvasSurface.mouseX, canvasSurface.mouseY )
                    onPressed: main.surfaceIn( canvasSurface.mouseX, canvasSurface.mouseY )
                    onReleased: main.surfaceOut( canvasSurface.mouseX, canvasSurface.mouseY )
                }

            }
        }

        Row {
            Button {
                id: cmdLeftButton
                onClicked: cmdLeftButton_clicked( )
                text: "Click"
                height: main.height / 4
                width: main.width / 2
            }

            Button {
                id: cmdRightButton
                onClicked: cmdRightButton_clicked( )
                text: "Anticlick"
                height: main.height / 4
                width: main.width / 2
            }
        }

        Rectangle  {
            id: txtCountdown
            height: main.height / 4
            width: main.width
            Text {
                id: lblCountdown
                anchors.verticalCenter: txtCountdown.verticalCenter
                anchors.horizontalCenter: txtCountdown.horizontalCenter
                font.pointSize: 80
                text: "00:00:00"

            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                     main.counting = 0
                        dlgCountdown.visible = true
                }

                onDoubleClicked: {
                    if ( main.counting == 1 ) {
                        main.counting = 0;
                    } else {
                        main.counting = 1;
                    }
                }
            }
        }


    }



        Timer {
            interval: 1000
            running: true
            repeat: true
            onTriggered: {
                var zh;
                var zm;
                var zs;

                if ( main.counting == 1 ) {
                    if ( main.hh < 10 )
                        zh = "0"
                    else
                        zh = "";

                    if ( main.mm < 10 )
                        zm = "0"
                    else
                        zm = "";

                    if ( main.ss < 10 )
                        zs = "0"
                    else
                        zs = "";

                    lblCountdown.text = zh + main.hh + ":" + zm+main.mm + ":" + zs+main.ss;
                    if ( ( main.hh > 0 ) || ( main.mm > 0 ) || ( main.ss > 0 ) ) {
                        if ( main.ss == 0 ) {
                            if ( main.mm == 0 ) {
                                if ( main.hh == 0 ) {
                                    main.counting = 0;
                                } else {
                                    main.mm = 59;
                                    main.hh = main.hh - 1;
                                }


                            } else {
                                main.ss = 59;
                                main.mm = main.mm - 1;
                            }
                        } else {
                            main.ss= main.ss - 1;
                        }


                    } else {
                        main.counting = 0;
                    }
                }

            }

        }


    Dialog {
        id: dlgCountdown
        visible: false
        title: "Countdown"
        standardButtons: StandardButton.Ok | StandardButton.Cancel
        Rectangle {
            Row {
                TextField {
                    id: txtHH
                    placeholderText: qsTr("HH")
                    width: 40
                }
                TextField {
                    id: txtMM
                    placeholderText: qsTr("MM")
                    width: 40                }
                TextField {
                    id: txtSS
                    placeholderText: qsTr("SS")
                    width: 40
                }
            }
        }
        onAccepted: {
            if ( txtHH.text !== "" )
                main.hh = txtHH.text
            else
                main.hh = 0;

            if ( main.mm !== "" )
                main.mm = txtMM.text
            else
                main.mm = 0;

            if ( main.ss !== "" )
                main.ss = txtSS.text
            else
                main.ss = 0

            main.counting  = 1
        }


    }


}
