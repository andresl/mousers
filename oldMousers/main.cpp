#include <QApplication>
#include <QLabel>

#include "view/PrincipalWidget.h"

int main( int argc, char *argv[] ) {
  QApplication app(argc, argv);

  PrincipalWidget *pw = new PrincipalWidget( );
  pw->show( );
  return app.exec( );
}

