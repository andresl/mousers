#include "ConfigDialog.h"
#include "PrincipalWidget.h"

ConfigDialog::ConfigDialog ( QWidget *parent ) {
    this->cmdAceptar = new QPushButton( "Aceptar", this );
    this->cmdCancelar = new QPushButton( "Cancelar", this );

    this->txtPuerto = new QLineEdit( this );

    this->txtPuerto->setText( QString::number( PrincipalWidget::CONFIG_PORT ) );

    this->lblHost = new QLabel( "IP: \n" + PrincipalWidget::CONFIG_HOST , this );
    this->lblPuerto = new QLabel( "Puerto:" , this );

    this->hl1 = new QHBoxLayout(  );
    this->hl2 = new QHBoxLayout(  );
    this->hl3 = new QHBoxLayout(  );
    this->vl = new QVBoxLayout( this );

    this->hl1->addWidget( this->lblHost );

    this->hl2->addWidget( this->lblPuerto );
    this->hl2->addWidget( this->txtPuerto );

    this->hl3->addWidget( this->cmdAceptar );
    this->hl3->addWidget( this->cmdCancelar );


    this->vl->addLayout( hl1 );
    this->vl->addLayout( hl2 );
    this->vl->addLayout( hl3 );

    connect( this->cmdAceptar, SIGNAL( clicked( ) ), this, SLOT( cambiarConfig( ) ) );
    connect( this->cmdCancelar, SIGNAL( clicked( ) ), this, SLOT( close( ) ) );

}


void ConfigDialog::cambiarConfig( ) {
    PrincipalWidget::CONFIG_PORT = this->txtPuerto->text( ).toInt( );
    this->accept( );

}
