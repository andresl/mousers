#include "PrincipalWidget.h"
#include <QDebug>
#include "ConfigDialog.h"
#include <QMessageBox>
#include <QTcpSocket>
#include <iostream>
#include <QNetworkInterface>


QString PrincipalWidget::CONFIG_HOST = QString( "" );
int PrincipalWidget::CONFIG_PORT = 7777;

PrincipalWidget::PrincipalWidget( QWidget *parent ) : QWidget ( parent ) {
    this->setWindowTitle( "Mousers Computer Control" );

    // layouts
    this->vl = new QVBoxLayout( this );
    this->hl = new QHBoxLayout( );
    this->hl2 = new QHBoxLayout( );
    this->ss = new QSpacerItem( 10, 1 );

    // botones
    this->cmdArriba = new QPushButton( "Arriba", this );
    this->cmdAbajo = new QPushButton( "Abajo", this );
    this->cmdDerecha = new QPushButton( "Derecha", this );
    this->cmdIzquierda = new QPushButton( "Izquierda", this );
    this->cmdConfig = new QPushButton( "Configuración", this );
    this->cmdClickIzq = new QPushButton( "Click ", this );
    this->cmdClickDer = new QPushButton( "Anticlick", this );


    // etiquetas
    this->lblEstado = new QLabel( "Desconectado", this );
    this->lblEstado->setMaximumHeight( 10 );

    this->vl->addWidget( this->lblEstado );
    this->vl->addWidget( this->cmdConfig );
    this->vl->addItem( this->ss );
    this->vl->addWidget( this->cmdArriba );
    this->vl->addLayout( this->hl );
    this->vl->addWidget( this->cmdAbajo );
    this->vl->addLayout( this->hl2 );

    this->hl->addWidget( this->cmdIzquierda );
    this->hl->addWidget( this->cmdDerecha );

    this->hl2->addWidget( this->cmdClickIzq );
    this->hl2->addWidget( this->cmdClickDer );


    this->setLayout( this->vl );

    connect( this->cmdConfig, SIGNAL( clicked( ) ), this, SLOT( abrirConfiguracion( ) ) );

    connect( this->cmdArriba, SIGNAL( clicked( ) ), this, SLOT( movArriba( ) ) ) ;
    connect( this->cmdAbajo, SIGNAL( clicked( ) ), this, SLOT( movAbajo( ) ) ) ;
    connect( this->cmdDerecha, SIGNAL( clicked( ) ), this, SLOT( movDerecha( ) ) ) ;
    connect( this->cmdIzquierda, SIGNAL( clicked( ) ), this, SLOT( movIzquierda( ) ) ) ;
    connect( this->cmdClickIzq, SIGNAL( clicked( ) ), this, SLOT( movClickIzquierdo( ) ) );
    connect( this->cmdClickDer, SIGNAL( clicked( ) ), this, SLOT( movClickDerecho   ( ) ) );

    //qDebug( ) << "Ready to go" ;



    this->srvMousers = new QTcpServer( this );
    connect( this->srvMousers, SIGNAL( newConnection() ), this, SLOT( nuevaConexion()));
    if ( !this->srvMousers->listen( QHostAddress::Any, PrincipalWidget::CONFIG_PORT ) ) {
        qDebug() << "Error";
        this->lblEstado->setText( "ERROR DE CONEXIÓN" );

    } else {
        qDebug() << "OK";
        this->lblEstado->setText( "LISTO" );
    }


    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)) {
            // qDebug() << "IP:" << address.toString();
            PrincipalWidget::CONFIG_HOST = PrincipalWidget::CONFIG_HOST + address.toString( ) + "\n";
        }

    }

}


void PrincipalWidget::nuevaConexion() {
    this->socketMousers = this->srvMousers->nextPendingConnection();

    socketMousers->write( "hola cliente" );
    socketMousers->flush();

}

void PrincipalWidget::abrirConfiguracion( ) {
    ConfigDialog *cd = new ConfigDialog( this );
    cd->setWindowState( cd->windowState( ) | Qt::WindowMaximized );
    cd->exec();
}



void PrincipalWidget::enviarMovimiento( PrincipalWidget::MovimientoRaton m ) {
    qDebug( ) << "Enviando: " << m;
    this->socketMousers->flush();

    this->socketMousers->write( QByteArray::number( m ) );
    this->socketMousers->flush();
}

void PrincipalWidget::movArriba( ) {
    this->enviarMovimiento( MOV_ARRIBA );
}

void PrincipalWidget::movAbajo( ) {
    this->enviarMovimiento( MOV_ABAJO );

}

void PrincipalWidget::movDerecha( ) {
    this->enviarMovimiento( MOV_DERECHA );

}

void PrincipalWidget::movIzquierda( ) {
    this->enviarMovimiento( MOV_IZQUIERDA );

}

void PrincipalWidget::movClickIzquierdo( ) {
    this->enviarMovimiento( CLICK_IZQUIERDO );
}

void PrincipalWidget::movClickDerecho( ) {
    this->enviarMovimiento( CLICK_DERECHO );
}
