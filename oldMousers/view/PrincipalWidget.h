#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QLabel>
#include <QSpacerItem>
#include <QString>
#include <QTcpServer>

class PrincipalWidget: public QWidget {
    Q_OBJECT
private:
    enum MovimientoRaton { MOV_ARRIBA, MOV_ABAJO, MOV_DERECHA, MOV_IZQUIERDA, CLICK_DERECHO, CLICK_IZQUIERDO };
    QPushButton *cmdArriba, *cmdAbajo, *cmdDerecha, *cmdIzquierda, *cmdConfig, *cmdClickIzq, *cmdClickDer;
    QVBoxLayout *vl;
    QHBoxLayout *hl, *hl2;
    QSpacerItem *ss;
    QLabel *lblEstado;
    QTcpServer *srvMousers;
    QTcpSocket *socketMousers;
    PrincipalWidget::MovimientoRaton movimientoActual;

public:
    PrincipalWidget( QWidget *parent = 0 );
    static QString CONFIG_HOST;
    static int CONFIG_PORT;

    void enviarMovimiento( PrincipalWidget::MovimientoRaton m ) ;

    // ~PrincipalWidget( );

public slots:
    void abrirConfiguracion( );
    void nuevaConexion();
    void movArriba( );
    void movAbajo( );
    void movDerecha( );
    void movIzquierda( );

    void movClickIzquierdo();
    void movClickDerecho();

};

