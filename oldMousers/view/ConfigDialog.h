#include <QDialog>
#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>


class ConfigDialog : public QDialog {
    Q_OBJECT
private:
    QLineEdit *txtPuerto;
    QLabel *lblHost, *lblPuerto;
    QVBoxLayout *vl;
    QHBoxLayout *hl1, *hl2, *hl3;
    QPushButton *cmdAceptar, *cmdCancelar;

public:
    ConfigDialog( QWidget *parent = 0 );

public slots:
    void cambiarConfig( );

};
