#include <QApplication>
#include "view/cview.h"
#include "view/chandler.h"

#include <QNetworkInterface>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    CView *c = new CView( );

    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)) {
            qDebug() << "IP:" << address.toString();
        }

    }

    return app.exec();
}
