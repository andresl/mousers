#ifndef CMOVE_H
#define CMOVE_H
#include <QPair>
#include <QDataStream>


class CMove {


public:
    enum MOV_TYPE {
        NONE,
        COORD,
        MOUSE_IN,
        MOUSE_OUT,
        RIGHT_CLICK,
        LEFT_CLICK
    };

    CMove();
    CMove( CMove::MOV_TYPE, int, int );
    CMove( int, int );
    CMove( CMove::MOV_TYPE, QPair< int, int > );

    ~CMove();

    void setMovType( CMove::MOV_TYPE );
    void setCoord( int, int );
    QPair< int, int > getCoord( );
    CMove::MOV_TYPE getMovType( );

private:


    MOV_TYPE movType;
    QPair< int, int > coord;


};


#endif // CMOVE_H
