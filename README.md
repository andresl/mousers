# README #

Mousers is a client-server application to control mouse from a mobile device. Functionality for keyboard control is planned in a future release.

Mousers' name is a reference to mice hunter robots from TMNT.

### What is this repository for? ###

* Quick summary

Source code in this repository is intended for deployment in mobile devices (server application). In order to get the client application, please refer to the [mousers-client repo](https://bitbucket.org/andresl/mousersclient).

* Version

Mousers is still in a pre-alpha stage. Code should build, but an unexpected action would lead to an application crash.


### How do I get set up? ###

Mousers is been developed in C++ and it uses the Qt Framework. 

You need the Qt Framework. This app should build in iOS and Android. You need the Android Native Development Kit (NDK) to get this app working in Android.